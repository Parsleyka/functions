const getSum = (str1, str2) => {
  if (typeof str1 === 'string' && typeof str2 === 'string' && !isNaN(str1) && !isNaN(str2)){
    return String(Number(str1) + Number(str2));
  }
  return false;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let numOfPosts = listOfPosts.filter((el) => {
    return el.author === authorName;
  }).length;

  let numOfComments = listOfPosts.reduce((num, el) => {
    if (el.hasOwnProperty('comments')){
      return num + el.comments.filter((elem) => {
        return elem.author === authorName;
      }).length;
    } else {
      return num;
    }
  }, 0);

  return `Post:${numOfPosts},comments:${numOfComments}`
};

const tickets = (people) => {
  let ticketPrice = 25;
  let currentMoney = {
    25: 0,
    50: 0,
    100: 0
  };

  for (const person of people) {
    if (person === ticketPrice){
      currentMoney[person]++;
    }else{
      let curr = person - ticketPrice;
      while (!currentMoney.hasOwnProperty(String(curr))){
        if (currentMoney[ticketPrice] !== 0){
          currentMoney[ticketPrice]--;
        }else{
          return 'NO';
        }
        curr -= ticketPrice;
      }
      if (currentMoney[curr] !== 0){
        currentMoney[curr]--;
      }else{
        return 'NO';
      }
      currentMoney[person]++;
    }
  }
  return 'YES';

};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
